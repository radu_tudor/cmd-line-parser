#COMMAND LINE PARSER

## Specification
input format:
 - string like arguments `"--foo bar"`

### 1. parse a `simple` flag
  given the following input:
  ```sh
  --foo
  ```
  the program should produce either a map or a JSON object as follows:
  ```JSON
  {"foo": true}
  ```

### 2. parse a `composite` flag
  given the following input:
  ```sh
  --foo bar
  ```
  the program should produce either a map or a JSON object as follows:
  ```JSON
  {"foo": "bar"}
  ```

### 3. parse a `composite` flag with numeric value
  given the following input:
  ```sh
  --number 1
  ```
  the program should produce either a map or a JSON object as follows:
  ```JSON
  {"number": 1}
  ```

### 4. parse multiple flags at once
  given the following input:
  ```sh
  --foo --bar baz --number 1
  ```
  the program should produce either a map or a JSON object as follows:
  ```JSON
  {"bar": "baz", "foo": true, "number": 1}